import pandas as pd
import numpy as np
import random as rd
import matplotlib.pyplot as plt

data = pd.read_csv('Productdata.csv')
data.head()

X = data[["Price (k$)","Sellnumber"]]
#Trực quan hóa các điểm dữ liệu
plt.scatter(X["Price (k$)"],X["Sellnumber"],c='black')
plt.xlabel('Price (k$)')
plt.ylabel('Sellnumber')
plt.show()
# Chọn số lượng cụm (k) và chọn trọng tâm ngẫu nhiên cho mỗi cụm

K=5

Centroids = (X.sample(n=K))
plt.scatter(X["Price (k$)"],X["Sellnumber"],c='black')
plt.scatter(Centroids["Price (k$)"],Centroids["Sellnumber"],c='red')
plt.xlabel('Price (k$)')
plt.ylabel('Sellnumber')
plt.show()
print(Centroids)

# Gán tất cả các điểm cho trọng tâm cụm gần nhất
# Tính toán lại các điểm của cụm mới được hình thành và lặp lại
diff = 1
j=0

while(diff!=0):
    XD=X
    i=1
    for index1,row_c in Centroids.iterrows():
        ED=[]
        for index2,row_d in XD.iterrows():
            d1=(row_c["Price (k$)"]-row_d["Price (k$)"])**2
            d2=(row_c["Sellnumber"]-row_d["Sellnumber"])**2
            d=np.sqrt(d1+d2)
            ED.append(d)
        X[i]=ED
        i=i+1

    C=[]
    for index,row in X.iterrows():
        min_dist=row[1]
        pos=1
        for i in range(K):
            if row[i+1] < min_dist:
                min_dist = row[i+1]
                pos=i+1
        C.append(pos)
    X["Cluster"]=C
    Centroids_new = X.groupby(["Cluster"]).mean()[["Sellnumber","Price (k$)"]]
    if j == 0:
        diff=1
        j=j+1
    else:
        diff = (Centroids_new['Sellnumber'] - Centroids['Sellnumber']).sum() + (Centroids_new['Price (k$)'] - Centroids['Price (k$)']).sum()
        print(diff.sum())
    Centroids = X.groupby(["Cluster"]).mean()[["Sellnumber","Price (k$)"]]
    
color=['blue','green','cyan','black','orange']
for k in range(K):
    data=X[X["Cluster"]==k+1]
    plt.scatter(data["Price (k$)"],data["Sellnumber"],c=color[k])
plt.scatter(Centroids["Price (k$)"],Centroids["Sellnumber"],c='red')
plt.xlabel('Price (k$)')
plt.ylabel('Sellnumber')
plt.show()